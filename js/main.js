jQuery.noConflict()(function ($) { // this was missing for me
    $(document).ready(function() { 

        // Cufon should be updated on resize
        $( window ).resize(function() {
                Cufon.replace('h1'); // Works without a selector engine
                Cufon.replace('#sub1'); // Requires a selector engine for IE 6-7, see above
                Cufon.replace('#bezoek, .social'); // Requires a selector engine for IE 6-7, see above
                Cufon.replace('.designer'); // Requires a selector engine for IE 6-7, see above
                Cufon.replace('h2'); // Requires a selector engine for IE 6-7, see above
                Cufon.replace('.design'); // Requires a selector engine for IE 6-7, see above
                Cufon.replace('.nav-container li a'); // Requires a selector engine for IE 6-7, see above
                Cufon.replace('.designersmall'); // Requires a selector engine for IE 6-7, see above

        })

       $('.mobile__menu ul:last-child').hide();
       var menuIsOpen = false;

       $('.mobile__menu ul:first-child').click(function() {
           if(!menuIsOpen) {
                $('.mobile__menu ul:last-child').slideDown("fast");
                menuIsOpen = true;
           } else {
                $('.mobile__menu ul:last-child').slideUp(500);
                menuIsOpen = false;
           }
       })

       $(window).scroll(function(){
           if ($(window).scrollTop() >= 220) {
                $('.mobile__menu').addClass('scrolled');
            }

            if ($(window).scrollTop() < 220) {
                $('.mobile__menu').removeClass('scrolled');
            }

            if ($(window).scrollTop() >= 330) {
                $('.mobile__menu').addClass('fixed');
            } else {
                $('.mobile__menu').removeClass('fixed');
            }
        });

       $('.menu__inner').hide();
       $('.menu__global__item').click(function() {

            var $target = $('html,body'); 

            var $this = $(this);

            if(!$(this).hasClass('active')) {
                $target.animate({scrollTop: $target.height()}, 500);
            }



           $(this).toggleClass('active');
           $(this).next().toggleClass('active');
           $(this).next().slideToggle(400);
       })

    });
});