var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('sass', function () {
  return gulp.src('./styles/src/**/main.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./styles/dist'));
});
 
gulp.task('sass:watch', function () {
  gulp.watch('./styles/src/**/*.scss', ['sass']);
});

gulp.task('default', ['sass', 'sass:watch']);
